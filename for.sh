echo '           Running folder rename *-pst to *-old....'
cd public
ls -al
for f in *-pst; do 
    mv -- "$f" "${f%-pst}-old"
done
ls -al
cd ..