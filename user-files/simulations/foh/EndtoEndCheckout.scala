package foh

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class EndtoEndCheckout extends Simulation {

	//environment variables
	var injectionProfile = System.getenv("sInjectionProfile")

	var userString = System.getenv("gUsers")
	var durationString = System.getenv("gDuration")
	var userRampString = System.getenv("gRamp")

	var users = Integer.parseInt(userString)
	var duration = Integer.parseInt(durationString)
	var userRamp = Integer.parseInt(userRampString)

	// hide password
	var password = System.getenv("gPass")

	val httpProtocol = http
		.baseUrl("sourceUrl")
		.inferHtmlResources()
		.acceptHeader("application/xml, text/xml, */*; q=0.01")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("en-US,en;q=0.5")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0")

	val headers_0 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_3 = Map("Accept" -> "*/*")

	val headers_4 = Map(
		"Accept" -> "application/json",
		"X-CSRF-Token" -> "fetch")

	val headers_9 = Map("Accept" -> "text/css,*/*;q=0.1")

	val headers_10 = Map(
		"Accept" -> "text/plain, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_11 = Map(
		"Accept" -> "application/json, text/javascript, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_16 = Map(
		"Accept" -> "application/atomsvc+xml;q=0.8, application/json;odata=fullmetadata;q=0.7, application/json;q=0.5, */*;q=0.1",
		"MaxDataServiceVersion" -> "3.0")

	val headers_30 = Map("X-Requested-With" -> "XMLHttpRequest")

	val headers_36 = Map(
		"Accept" -> "*/*",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_37 = Map(
		"Accept" -> "application/json, text/javascript, */*; q=0.01",
		"X-CSRF-Token" -> "Fetch",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_38 = Map(
		"Accept" -> "application/xml",
		"Accept-Language" -> "en",
		"MaxDataServiceVersion" -> "3.0",
		"sap-contextid-accept" -> "header")

	val headers_39 = Map(
		"Accept" -> "application/json",
		"Accept-Language" -> "en",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header",
		"x-csrf-token" -> "Fetch")

	val headers_40 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_c6bb-0048-a3ab",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_57 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_3a64-78b7-fb9a",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_64 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_2b28-e847-6c53",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_89 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_55c3-166f-2020",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_90 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_0478-6e93-dccb",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_91 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_3e43-1343-3443",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_92 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_2316-ff20-0797",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_96 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_f04c-fc08-0879",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_97 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_b357-2141-39a8",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_101 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_6b53-d2f4-8360",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_102 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_8571-4387-1b3e",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_103 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_d7be-d983-981f",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_104 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_2e4f-6869-5af8",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_105 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_c8bd-1a34-b60d",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_106 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_8aae-a82e-55b6",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_107 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_7097-e8cf-7362",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_110 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_338e-cddf-9cc7",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_111 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_59ea-2ec3-59c0",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_112 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_c389-35b6-fae7",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_115 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_ad6a-59e8-1745",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_116 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_0dab-0443-880a",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_117 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_3062-b001-7e0f",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_118 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_5866-e422-bc89",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_119 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_49b3-8d32-e8b2",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "sourceUrl",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")



	val scn = scenario("EndtoEndCheckout")
		.exec(http("HANA Login")
			.get("/sap/hana/uis/clients/ushell-app/shells/fiori/NewFioriLaunchpad.html?siteId=dhs|foh|ui|uis|appsites|foh")
			.headers(headers_0)
			.basicAuth("userName",password)
			.resources(http("Bootstrap")
			.get("/sap/hana/uis/clients/flp/bootstrap/uis_fiori.js?ts=201607271029")
			.headers(headers_3)))

            .exec(http("walkInParameters")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_57)
			.body(RawFileBody("foh/endtoendcheckout/walkInParameters.dat")))
			.pause(1)

		.group("Waitroom") {
			exec(http("Waitroom view")
				.get("/dhs/foh/ui/components/waitroom/view/Waitroom.view.xml?ts=201607271029")
				.headers(headers_30)
				.resources(http("Waitroom controller")
				.get("/dhs/foh/ui/components/waitroom/controller/Waitroom.controller.js?ts=201607271029")
				.headers(headers_10),
				http("WaitroomTable controller")
				.get("/dhs/foh/ui/components/waitroom/controller/fragments/WaitroomTablePerso.controller.js?ts=201607271029")
				.headers(headers_10),
				http("OrgAssignment controller")
				.get("/dhs/foh/ui/components/waitroom/controller/fragments/OrgAssignments.controller.js?ts=201607271029")
				.headers(headers_10),
				http("AppointmentsTable frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/Waitroom/AppointmentsTable.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("WaitroomTable frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/Waitroom/WaitroomTable.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("foh-waitroom")
				.get("/sap/hana/uis/odata/uis_nav_data.xsodata/UIS_US_NAVIGATIONParameters(IN_TAG='foh-waitroom')/Results?$format=json&$top=1")
				.headers(headers_16),
				http("Component preload")
				.get("/dhs/foh/ui/components/waitroom/Component-preload.js")
				.headers(headers_3),
				http("Component js")
				.get("/dhs/foh/ui/components/waitroom/Component.js?ts=201607271029")
				.headers(headers_10),
				http("Waitroom util")
				.get("/dhs/foh/ui/components/waitroom/util/util.js?ts=201607271029")
				.headers(headers_10),
				http("Common util")
				.get("/dhs/foh/ui/common/utilities/util.js?ts=201607271029")
				.headers(headers_10),
				http("persoServiceTemplate")
				.get("/dhs/foh/ui/common/patterns/persoServiceTemplate.js?ts=201607271029")
				.headers(headers_10),
				http("Waitroom manifest")
				.get("/dhs/foh/ui/components/waitroom/manifest.json?ts=201607271029")
				.headers(headers_11),
				http("Waitroom messageBundle_en")
				.get("/dhs/foh/ui/components/waitroom/i18n/messageBundle_en.properties")
				.headers(headers_10),
				http("Waitroom messageBundle")
				.get("/dhs/foh/ui/components/waitroom/i18n/messageBundle.properties")
				.headers(headers_10),
				http("Waitroom css")
				.get("/dhs/foh/ui/components/waitroom/css/waitroom.css?ts=201607271029")
				.headers(headers_9),
				http("Waitroom App view")
				.get("/dhs/foh/ui/components/waitroom/view/App.view.xml?ts=201607271029")
				.headers(headers_30),
				http("Waitroom App controller")
				.get("/dhs/foh/ui/components/waitroom/controller/App.controller.js?ts=201607271029")
				.headers(headers_10),
				http("Waitroom Base controller")
				.get("/dhs/foh/ui/components/waitroom/controller/Base.controller.js?ts=201607271029")
				.headers(headers_10),
				http("waitroom schema")
				.get("/dhs/foh/ui/components/waitroom/util/schema.js?ts=201607271029")
				.headers(headers_10),
				http("Waitroom WalkinFlags")
				.get("/dhs/foh/ui/components/waitroom/controller/fragments/WalkinFlags.js?ts=201607271029")
				.headers(headers_10),
				http("InterpreterLanguageDialog")
				.get("/dhs/foh/ui/components/waitroom/controller/fragments/InterpreterLanguageDialog.controller.js?ts=201607271029")
				.headers(headers_10),
				http("UIAuthentication")
				.get("/dhs/foh/data/services/xsjs/common/UIAuthentication.xsjs?appName=WAITROOM&ts=201607271029")
				.headers(headers_36),
				http("GetServerInfo")
				.get("/sap/es/ina/GetServerInfo?_=1584337014263")
				.headers(headers_37)
				.check(status.is(404)),
				http("language_metadata")
				.get("/dhs/foh/data/services/xsodata/waitroom.xsodata/$metadata?sap-language=EN")
				.headers(headers_38),
				http("Waitroom services batch")
				.get("/dhs/foh/data/services/xsodata/waitroom.xsodata/")
				.headers(headers_39),
				http("getConfig")
				.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
				.headers(headers_40)
				.body(RawFileBody("foh/endtoendcheckout/getConfig.dat")),
				http("WalkinProcessing view")
				.get("/dhs/foh/ui/components/waitroom/view/WalkinProcessing.view.xml?ts=201607271029")
				.headers(headers_30),
				http("WalkinProcessing controller")
				.get("/dhs/foh/ui/components/waitroom/controller/WalkinProcessing.controller.js?ts=201607271029")
				.headers(headers_10),
				http("Waitroom formatter")
				.get("/dhs/foh/ui/components/waitroom/util/formatter.js?ts=201607271029")
				.headers(headers_10),
				http("CustomerDetails js")
				.get("/dhs/foh/ui/components/waitroom/controller/fragments/CustomerDetails.js?ts=201607271029")
				.headers(headers_10),
				http("WalkinItemDialog controller")
				.get("/dhs/foh/ui/components/waitroom/controller/fragments/WalkinItemDialog.controller.js?ts=201607271029")
				.headers(headers_10),
				http("CustomerDetails frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("ObjectPageLayout")
				.get("/dhs/foh/ui/components/waitroom/controls/ObjectPageLayout.js?ts=201607271029")
				.headers(headers_10),
				http("WalkinDetails frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/WalkinDetails.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("WalkinFlags frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/WalkinFlags.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("WalkinItemsTable frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/WalkinProcessing/WalkinItemsTable.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("CustomerDetails Indicators")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/Indicators.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("CustomerDetails PreviousWalkins")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/PreviousWalkins.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("CustomerDetails Relationships")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/Relationships.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("CustomerDetails Appointments")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/Appointments.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("CustomerDetails WorkItems")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/WorkItems.fragment.xml?ts=201607271029")
				.headers(headers_30)))
		}

		.exec(http("walkInParameters batch")
				.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
				.headers(headers_64)
				.body(RawFileBody("foh/endtoendcheckout/walkInParametersBatch.dat")))
				.pause(1)

		.group("Create Walkin") {
			exec(http("Create Walkin view")
				.get("/dhs/foh/ui/components/waitroom/view/CreateWalkin.view.xml?ts=201607271029")
				.headers(headers_30)
				.resources(http("Create Walkin controller")
				.get("/dhs/foh/ui/components/waitroom/controller/CreateWalkin.controller.js?ts=201607271029")
				.headers(headers_10),
				http("Wizard control")
				.get("/dhs/foh/ui/components/waitroom/controls/Wizard.js?ts=201607271029")
				.headers(headers_10),
				http("WizardStep control")
				.get("/dhs/foh/ui/components/waitroom/controls/WizardStep.js?ts=201607271029")
				.headers(headers_10),
				http("CustomerSearchStep frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CreateWalkin/CustomerSearchStep.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("DatePicker control")
				.get("/dhs/foh/ui/components/waitroom/controls/DatePicker.js?ts=201607271029")
				.headers(headers_10),
				http("Input control")
				.get("/dhs/foh/ui/components/waitroom/controls/Input.js?ts=201607271029")
				.headers(headers_10),
				http("CustomerSearchTable frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CreateWalkin/CustomerSearchTable.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("CustomerDetailStep frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CreateWalkin/CustomerDetailsStep.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("CustomerDetails frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("WalkInDetails frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/WalkinDetails.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("WalkInFlags frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/WalkinFlags.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("WalkinItemsTable frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/WalkinProcessing/WalkinItemsTable.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("Indicators frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/Indicators.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("PreviousWalkins frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/PreviousWalkins.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("Relationships frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/Relationships.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("Appointments frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/Appointments.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("WorkItems frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CustomerDetails/WorkItems.fragment.xml?ts=201607271029")
				.headers(headers_30),
				http("WalkinDetailsStep frag")
				.get("/dhs/foh/ui/components/waitroom/view/fragments/CreateWalkin/WalkinDetailsStep.fragment.xml?ts=201607271029")
				.headers(headers_30)))
		}

		.exec(http("walkInCRUD")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_89)
			.body(RawFileBody("foh/endtoendcheckout/walkInCRUD.dat")))
		.pause(1)

		.exec(http("customerSearch")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_90)
			.body(RawFileBody("foh/endtoendcheckout/customerSearch.dat")))
		.pause(1)

		.exec(http("customerDetails")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_91)
			.body(RawFileBody("foh/endtoendcheckout/customerDetails.dat"))
			.resources(http("customerParticipation")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_92)
			.body(RawFileBody("foh/endtoendcheckout/customerParticipation.dat")),
            http("CaaS cims")
			.get("/dhs/cims/data/services/xsjs/CaaS.xsjs?crn=505987458A&ts=201607271029")
			.headers(headers_36),
            http("getCustOverview")
			.get("/dhs/foh/data/services/xsjs/co/getCustOverview.xsjs?crn=505987458A&site=61021264&ts=201607271029")
			.headers(headers_36)))
		.pause(1)

		.exec(http("customerPartner")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_96)
			.body(RawFileBody("foh/endtoendcheckout/customerPartner.dat")))
		.pause(1)

		.exec(http("walkInHistory")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_97)
			.body(RawFileBody("foh/endtoendcheckout/walkInHistory.dat"))
			.resources(http("getCustAppointments xsjs")
			.get("/dhs/foh/data/services/xsjs/appointments/getCustAppointments.xsjs?crn=505987458A&ts=201607271029")
			.headers(headers_36)))
		.pause(1)

		.exec(http("getCustWorkItems xsjs")
			.get("/dhs/foh/data/services/xsjs/wlm/getCustWorkItems.xsjs?crn=505987458A&ts=201607271029")
			.headers(headers_36))
		.pause(1)

		.exec(http("InterpreterLanguageDialog frag")
			.get("/dhs/foh/ui/components/waitroom/view/fragments/InterpreterLanguageDialog.fragment.xml?ts=201607271029")
			.headers(headers_30)
			.resources(http("IMSLangBatch")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_101)
			.body(RawFileBody("foh/endtoendcheckout/IMSLangBatch.dat"))))
		.pause(1)

		.exec(http("walkInCRUD_PUT")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_102)
			.body(RawFileBody("foh/endtoendcheckout/walkInCRUD_PUT.dat"))
			.resources(http("walkInItem_POST")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_103)
			.body(RawFileBody("foh/endtoendcheckout/walkInItem_POST.dat")),
            http("walkInFlag_POST")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_104)
			.body(RawFileBody("foh/endtoendcheckout/walkInFlag_POST.dat")),
            http("walkInItems_GET")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_105)
			.body(RawFileBody("foh/endtoendcheckout/walkInItems_GET.dat"))))
		.pause(1)

		.exec(http("walkInParameter_GET")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_106)
			.body(RawFileBody("foh/endtoendcheckout/walkInParameter_GET.dat"))
			.resources(http("custParameters_GET")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_107)
			.body(RawFileBody("foh/endtoendcheckout/custParameters_GET.dat")),
            http("getCustOverview2")
			.get("/dhs/foh/data/services/xsjs/co/getCustOverview.xsjs?crn=505987458A&site=61021264&ts=201607271029")
			.headers(headers_36)))
		.pause(1)

		.exec(http("cimsDataService")
			.get("/dhs/cims/data/services/xsjs/CaaS.xsjs?crn=505987458A&ts=201607271029")
			.headers(headers_36)
			.resources(http("custParticipation_GET")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_110)
			.body(RawFileBody("foh/endtoendcheckout/custParticipation_GET.dat"))))
		.pause(1)

		.exec(http("walkInHistoryParameters")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_111)
			.body(RawFileBody("foh/endtoendcheckout/walkInHistoryParameters.dat")))
		.pause(1)

		.exec(http("customerNominee")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_112)
			.body(RawFileBody("foh/endtoendcheckout/customerNominee.dat")))
		.pause(1)

		.exec(http("getCustAppointments_xsjs")
			.get("/dhs/foh/data/services/xsjs/appointments/getCustAppointments.xsjs?crn=505987458A&ts=201607271029")
			.headers(headers_36))
		.pause(1)

		.exec(http("getCustWorkItems_xsjs")
			.get("/dhs/foh/data/services/xsjs/wlm/getCustWorkItems.xsjs?crn=505987458A&ts=201607271029")
			.headers(headers_36))
		.pause(1)

		.exec(http("walkIn_PUT")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_115)
			.body(RawFileBody("foh/endtoendcheckout/walkIn_PUT.dat")))
		.pause(1)

		.exec(http("walkIn_GET")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_116)
			.body(RawFileBody("foh/endtoendcheckout/walkIn_GET.dat")))
		.pause(1)

		.exec(http("walkInCRUD2_PUT")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_117)
			.body(RawFileBody("foh/endtoendcheckout/walkInCRUD2_PUT.dat"))
			.resources(http("walkInItems_PUT")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_118)
			.body(RawFileBody("foh/endtoendcheckout/walkInItems_PUT.dat")),
            http("walkInParameters_GET")
			.post("/dhs/foh/data/services/xsodata/waitroom.xsodata/$batch")
			.headers(headers_119)
			.body(RawFileBody("foh/endtoendcheckout/walkInParameters_GET.dat"))))

	//fragment group
	// .group("fragmentGroup") {
	// 		exec(http("fragmentGG")
	// 			.get("/dhs/foh/ui/components/waitroom/view/Waitroom.view.xml?ts=201607271029")
	// 			.headers(headers_30)
	// 			.resources(http("Waitroom controller")
	// 			.get("/dhs/foh/ui/components/waitroom/controller/Waitroom.controller.js?ts=201607271029")
	// 			.headers(headers_10),
	// 		))
	// }

	if (injectionProfile.equals("constantUser")) {
		setUp(scn.inject(constantUsersPerSec(users) during (duration seconds))).protocols(httpProtocol)
	} else if (injectionProfile.equals("heavyUser")) {
		setUp(scn.inject(heavisideUsers(users) during(duration minutes))).protocols(httpProtocol)
	} else if (injectionProfile.equals("rampUser")) {
		setUp(scn.inject(rampConcurrentUsers(users) to(userRamp) during(duration minutes))).protocols(httpProtocol)
	}
}