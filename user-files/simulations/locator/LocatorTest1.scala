package locator

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class LocatorTest1 extends Simulation {

	//environment variables
	var injectionProfile = System.getenv("sInjectionProfile")

	var userString = System.getenv("gUsers")
	var durationString = System.getenv("gDuration")
	var userRampString = System.getenv("gRamp")

	var users = Integer.parseInt(userString)
	var duration = Integer.parseInt(durationString)
	var userRamp = Integer.parseInt(userRampString)

	// hide password
	var password = System.getenv("gPass")

	val httpProtocol = http
		.baseUrl("url-link")
		.inferHtmlResources(BlackList(""".*.css""", """.*.js""", """.*.ico"""), WhiteList())
		.acceptHeader("application/xml, text/xml, */*; q=0.01")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("en-US,en;q=0.5")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0")

	val headers_0 = Map("X-Requested-With" -> "XMLHttpRequest")

	val headers_1 = Map(
		"Accept" -> "text/plain, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_9 = Map(
		"Accept" -> "application/json",
		"Accept-Language" -> "en",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header",
		"x-csrf-token" -> "Fetch")

	val headers_10 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_cfe3-c70b-0180",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_11 = Map(
		"Accept" -> "*/*",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_60 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_61 = Map("Accept" -> "*/*")

	val headers_65 = Map(
		"Accept" -> "application/json",
		"X-CSRF-Token" -> "fetch")

	val headers_67 = Map("X-Requested-With" -> "XMLHttpRequest")

	val headers_68 = Map(
		"Accept" -> "application/json, text/javascript, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_612 = Map("Accept" -> "text/css,*/*;q=0.1")

	val headers_615 = Map(
		"Accept" -> "application/atomsvc+xml;q=0.8, application/json;odata=fullmetadata;q=0.7, application/json;q=0.5, */*;q=0.1",
		"MaxDataServiceVersion" -> "3.0")

	val headers_616 = Map("Accept" -> "image/webp,*/*")

	val headers_626 = Map(
		"Accept" -> "application/xml, text/xml, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_641 = Map(
		"Accept" -> "application/xml",
		"Accept-Language" -> "en",
		"MaxDataServiceVersion" -> "3.0",
		"sap-contextid-accept" -> "header")

	val headers_642 = Map(
		"Accept" -> "application/json",
		"Accept-Language" -> "en",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header",
		"x-csrf-token" -> "Fetch")

	val headers_643 = Map(
		"Accept" -> "application/json, text/javascript, */*; q=0.01",
		"X-CSRF-Token" -> "Fetch",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_644 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_cea0-c50d-8dcb",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_645 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_9a42-7734-8c63",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_80 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_81 = Map(
		"Accept" -> "application/json",
		"X-CSRF-Token" -> "fetch")

	val headers_82 = Map("X-Requested-With" -> "XMLHttpRequest")

	val headers_83 = Map(
		"Accept" -> "application/atomsvc+xml;q=0.8, application/json;odata=fullmetadata;q=0.7, application/json;q=0.5, */*;q=0.1",
		"MaxDataServiceVersion" -> "3.0")

	val headers_87 = Map(
		"Accept" -> "application/json, text/javascript, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_811 = Map("Accept" -> "text/css,*/*;q=0.1")

	val headers_813 = Map(
		"Accept" -> "application/xml, text/xml, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_832 = Map(
		"Accept" -> "application/json, text/javascript, */*; q=0.01",
		"X-CSRF-Token" -> "Fetch",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_833 = Map(
		"Accept" -> "application/xml",
		"Accept-Language" -> "en",
		"MaxDataServiceVersion" -> "3.0",
		"sap-contextid-accept" -> "header")

	val headers_834 = Map(
		"Accept" -> "application/json",
		"Accept-Language" -> "en",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header",
		"x-csrf-token" -> "Fetch")

	val headers_835 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_d9a2-50ca-f3f4",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_836 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_a799-223b-b02b",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_837 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_f28a-2bf7-fd12",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val scn = scenario("LocatorTest1")
	.group("Search"){
		exec(http("searchResultsView")
			.get("/dhs/ol/ui/components/search/view/SearchResults.view.xml?ts=201607271029")
			.headers(headers_0)
			.basicAuth("user-name",password)
			.resources(http("resultsController")
			.get("/dhs/ol/ui/components/search/controller/SearchResults.controller.js?ts=201607271029")
			.headers(headers_1),
            http("quickLinksFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/QuickLinksButton.fragment.xml?ts=201607271029")
			.headers(headers_0),
            http("buttonsFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Lists/Buttons.fragment.xml?ts=201607271029")
			.headers(headers_0),
            http("resultsContentFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/SearchResults/SearchResultsContent.fragment.xml?ts=201607271029")
			.headers(headers_0),
            http("orgsFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/SearchResults/Filterbar/Orgs.fragment.xml?ts=201607271029")
			.headers(headers_0),
            http("servicesFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/SearchResults/Filterbar/Services.fragment.xml?ts=201607271029")
			.headers(headers_0),
            http("placesFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/SearchResults/Filterbar/Places.fragment.xml?ts=201607271029")
			.headers(headers_0),
            http("orgsResultFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/SearchResults/ResultTemplates/Orgs.fragment.xml?ts=201607271029")
			.headers(headers_0),
            http("GETOData")
			.get("/dhs/ol/data/services/xsodata/ol.xsodata/")
			.headers(headers_9),
            http("GETBatch")
			.post("/dhs/ol/data/services/xsodata/ol.xsodata/$batch")
			.headers(headers_10)
			.body(RawFileBody("locator/locatortest1/searchResultsView.dat")),
            http("GETLog")
			.get("/dhs/ol/data/services/xsjs/log.xsjs?searchType=Orgs&searchQuery=a&results=13506&responseTime=1.601")
			.headers(headers_11)
			.check(status.is(200))))
	}

	.pause(3)
	//Colocation
	.group("Colocation"){
		exec(http("HANALogin")
			.get("/sap/hana/uis/clients/ushell-app/shells/fiori/NewFioriLaunchpad.html?siteId=dhs|foh|ui|uis|appsites|foh")
			.headers(headers_60)
			.resources(http("bootstrap")
			.get("/sap/hana/uis/clients/flp/bootstrap/uis_fiori.js?ts=201607271029")
			.headers(headers_61),
            http("fioriCore1")
			.get("/dhs/ui5/1.48.11/resources/sap/fiori/core-min-2.js?ts=201607271029")
			.headers(headers_61),
            http("fioriCore2")
			.get("/dhs/ui5/1.48.11/resources/sap/fiori/core-min-0.js?ts=201607271029")
			.headers(headers_61),
            http("fioriCore3")
			.get("/dhs/ui5/1.48.11/resources/sap/fiori/core-min-3.js?ts=201607271029")
			.headers(headers_61),
            http("getLaunchpad")
			.get("/sap/hana/uis/server/rest/v2/sites/dhs%7Cfoh%7Cui%7Cuis%7Cappsites%7Cfoh?_=1584336750307")
			.headers(headers_65),
            http("fioriCore4")
			.get("/dhs/ui5/1.48.11/resources/sap/fiori/core-min-1.js?ts=201607271029")
			.headers(headers_61),
            http("libraryJS")
			.get("/sap/hana/uis/clients/flp/js/sap/hana/uis/flp/library-preload.js?ts=201607271029")
			.headers(headers_67)
			.check(status.is(404)),
            http("libraryJSON")
			.get("/sap/hana/uis/clients/flp/js/sap/hana/uis/flp/library-preload.json?ts=201607271029")
			.headers(headers_68),
            http("fiori-i18n")
			.get("/dhs/ui5/1.48.11/resources/sap/fiori/messagebundle-preload_en.js?ts=201607271029")
			.headers(headers_67),
            http("applauncherChip")
			.get("/sap/hana/uis/clients/flp/external-files/chips/applauncher.chip.xml")
			.headers(headers_61),
            http("dynamicChip")
			.get("/sap/hana/uis/clients/flp/external-files/chips/applauncher_dynamic.chip.xml")
			.headers(headers_61),
            http("libraryCSS")
			.get("/dhs/ui5/1.48.11/resources/sap/fiori/themes/sap_belize_plus/library.css?ts=201607271029")
			.headers(headers_612),
            http("libraryParam")
			.get("/dhs/ui5/1.48.11/resources/sap/fiori/themes/sap_belize_plus/library-parameters.json?ts=201607271029")
			.headers(headers_68),
            http("userInfoJS")
			.get("/dhs/ui5/1.48.11/resources/sap/ushell/services/UserInfo.js?ts=201607271029")
			.headers(headers_67),
            http("getOData")
			.get("/sap/hana/uis/odata/uis_nav_data.xsodata/UIS_US_NAVIGATIONParameters(IN_TAG='ol-search')/Results?$format=json&$top=1")
			.headers(headers_615),
            http("themes")
			.get("/dhs/ui5/1.48.11/resources/sap/ushell/themes/base/img/sap_55x27.png")
			.headers(headers_616),
            http("componentJS")
			.get("/dhs/ol/ui/components/search/Component.js?ts=201607271029")
			.headers(headers_67),
            http("searchUtilJS")
			.get("/dhs/ol/ui/components/search/util/util.js?ts=201607271029")
			.headers(headers_67),
            http("utilJS")
			.get("/dhs/ol/ui/common/utilities/util.js?ts=201607271029")
			.headers(headers_67),
            http("manifest1")
			.get("/dhs/ol/ui/components/search/manifest.json?ts=201607271029")
			.headers(headers_68),
            http("manifest2")
			.get("/dhs/ol/ui/components/search/manifest.json?ts=201607271029")
			.headers(headers_68),
            http("i18nen")
			.get("/dhs/ol/ui/components/search/i18n/messageBundle_en.properties")
			.headers(headers_67),
            http("i18n")
			.get("/dhs/ol/ui/components/search/i18n/messageBundle.properties")
			.headers(headers_67),
            http("olCSS")
			.get("/dhs/ol/ui/components/search/css/ol.css?ts=201607271029")
			.headers(headers_612),
            http("i18nen2")
			.get("/dhs/ol/ui/components/search/i18n/messageBundle_en.properties")
			.headers(headers_67),
            http("appView")
			.get("/dhs/ol/ui/components/search/view/App.view.xml?ts=201607271029")
			.headers(headers_626),
            http("appController")
			.get("/dhs/ol/ui/components/search/controller/App.controller.js?ts=201607271029")
			.headers(headers_67),
            http("baseContoller")
			.get("/dhs/ol/ui/components/search/controller/Base.controller.js?ts=201607271029")
			.headers(headers_67),
            http("orgView")
			.get("/dhs/ol/ui/components/search/view/Org.view.xml?ts=201607271029")
			.headers(headers_626),
            http("orgController")
			.get("/dhs/ol/ui/components/search/controller/Org.controller.js?ts=201607271029")
			.headers(headers_67),
            http("formatter")
			.get("/dhs/ol/ui/components/search/util/formatter.js?ts=201607271029")
			.headers(headers_67),
            http("objectPageLayout")
			.get("/dhs/ol/ui/components/search/controls/ObjectPageLayout.js?ts=201607271029")
			.headers(headers_67),
            http("infoFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Info.fragment.xml?ts=201607271029")
			.headers(headers_626),
            http("staffFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Staff.fragment.xml?ts=201607271029")
			.headers(headers_626),
            http("orgStructureFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/OrgStructure.fragment.xml?ts=201607271029")
			.headers(headers_626),
            http("colocatedFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Colocated.fragment.xml?ts=201607271029")
			.headers(headers_626),
            http("nearbyFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Nearby.fragment.xml?ts=201607271029")
			.headers(headers_626),
            http("facilitiesFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Facilities.fragment.xml?ts=201607271029")
			.headers(headers_626),
            http("notesFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Notes.fragment.xml?ts=201607271029")
			.headers(headers_626),
            http("i18n2")
			.get("/dhs/ol/ui/components/search/i18n/messageBundle.properties")
			.headers(headers_67),
            http("metadata")
			.get("/dhs/ol/data/services/xsodata/ol.xsodata/$metadata?sap-language=EN")
			.headers(headers_641),
            http("GETOData2")
			.get("/dhs/ol/data/services/xsodata/ol.xsodata/")
			.headers(headers_642),
            http("serverInfo")
			.get("/sap/es/ina/GetServerInfo?_=1584336750518")
			.headers(headers_643)
			.check(status.is(404)),
            http("getOrgDetails")
			.post("/dhs/ol/data/services/xsodata/ol.xsodata/$batch")
			.headers(headers_644)
			.body(RawFileBody("locator/locatorcolocation1/GETOrgDetails.dat"))))
		.pause(4)
		.exec(http("getColocated")
			.post("/dhs/ol/data/services/xsodata/ol.xsodata/$batch")
			.headers(headers_645)
			.body(RawFileBody("locator/locatorcolocation1/GETColocated.dat")))
	}

	.pause(3)
	//Nearby
	.group("Nearby") {
		exec(http("HANALogin")
			.get("/sap/hana/uis/clients/ushell-app/shells/fiori/NewFioriLaunchpad.html?siteId=dhs|foh|ui|uis|appsites|foh")
			.headers(headers_80)
			.resources(http("getLaunchpad")
			.get("/sap/hana/uis/server/rest/v2/sites/dhs%7Cfoh%7Cui%7Cuis%7Cappsites%7Cfoh?_=1584336526820")
			.headers(headers_81),
            http("libPreload")
			.get("/sap/hana/uis/clients/flp/js/sap/hana/uis/flp/library-preload.js?ts=201607271029")
			.headers(headers_82)
			.check(status.is(404)),
            http("getOData")
			.get("/sap/hana/uis/odata/uis_nav_data.xsodata/UIS_US_NAVIGATIONParameters(IN_TAG='ol-search')/Results?$format=json&$top=1")
			.headers(headers_83),
            http("ComponentJS")
			.get("/dhs/ol/ui/components/search/Component.js?ts=201607271029")
			.headers(headers_82),
            http("searchUtilJS")
			.get("/dhs/ol/ui/components/search/util/util.js?ts=201607271029")
			.headers(headers_82),
            http("utilJS")
			.get("/dhs/ol/ui/common/utilities/util.js?ts=201607271029")
			.headers(headers_82),
            http("searchManifest")
			.get("/dhs/ol/ui/components/search/manifest.json?ts=201607271029")
			.headers(headers_87),
            http("searchManifest2")
			.get("/dhs/ol/ui/components/search/manifest.json?ts=201607271029")
			.headers(headers_87),
            http("i18nen")
			.get("/dhs/ol/ui/components/search/i18n/messageBundle_en.properties")
			.headers(headers_82),
            http("i18n")
			.get("/dhs/ol/ui/components/search/i18n/messageBundle.properties")
			.headers(headers_82),
            http("css")
			.get("/dhs/ol/ui/components/search/css/ol.css?ts=201607271029")
			.headers(headers_811),
            http("i18nen2")
			.get("/dhs/ol/ui/components/search/i18n/messageBundle_en.properties")
			.headers(headers_82),
            http("appView")
			.get("/dhs/ol/ui/components/search/view/App.view.xml?ts=201607271029")
			.headers(headers_813),
            http("appController")
			.get("/dhs/ol/ui/components/search/controller/App.controller.js?ts=201607271029")
			.headers(headers_82),
            http("baseController")
			.get("/dhs/ol/ui/components/search/controller/Base.controller.js?ts=201607271029")
			.headers(headers_82),
            http("orgView")
			.get("/dhs/ol/ui/components/search/view/Org.view.xml?ts=201607271029")
			.headers(headers_813),
            http("orgController")
			.get("/dhs/ol/ui/components/search/controller/Org.controller.js?ts=201607271029")
			.headers(headers_82),
            http("formatterJS")
			.get("/dhs/ol/ui/components/search/util/formatter.js?ts=201607271029")
			.headers(headers_82),
            http("objectPageLayout")
			.get("/dhs/ol/ui/components/search/controls/ObjectPageLayout.js?ts=201607271029")
			.headers(headers_82),
            http("objectPageHeader")
			.get("/dhs/ui5/1.48.11/resources/sap/uxap/ObjectPageHeader.js?ts=201607271029")
			.headers(headers_82),
            http("actionButton")
			.get("/dhs/ui5/1.48.11/resources/sap/uxap/ObjectPageHeaderActionButton.js?ts=201607271029")
			.headers(headers_82),
            http("infoFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Info.fragment.xml?ts=201607271029")
			.headers(headers_813),
            http("staffFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Staff.fragment.xml?ts=201607271029")
			.headers(headers_813),
            http("orgFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/OrgStructure.fragment.xml?ts=201607271029")
			.headers(headers_813),
            http("colocatedFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Colocated.fragment.xml?ts=201607271029")
			.headers(headers_813),
            http("nearbyFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Nearby.fragment.xml?ts=201607271029")
			.headers(headers_813),
            http("facilitiesFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Facilities.fragment.xml?ts=201607271029")
			.headers(headers_813),
            http("notesFrag")
			.get("/dhs/ol/ui/components/search/view/fragments/Org/Notes.fragment.xml?ts=201607271029")
			.headers(headers_813),
            http("objectPageContent")
			.get("/dhs/ui5/1.48.11/resources/sap/uxap/ObjectPageHeaderContent.js?ts=201607271029")
			.headers(headers_82),
            http("i18n2")
			.get("/dhs/ol/ui/components/search/i18n/messageBundle.properties")
			.headers(headers_82),
            http("objectPageRenderer")
			.get("/dhs/ui5/1.48.11/resources/sap/uxap/ObjectPageHeaderContentRenderer.js?ts=201607271029")
			.headers(headers_82),
            http("getServerInfo")
			.get("/sap/es/ina/GetServerInfo?_=1584336526914")
			.headers(headers_832)
			.check(status.is(404)),
            http("getMetadata")
			.get("/dhs/ol/data/services/xsodata/ol.xsodata/$metadata?sap-language=EN")
			.headers(headers_833),
            http("getOData2")
			.get("/dhs/ol/data/services/xsodata/ol.xsodata/")
			.headers(headers_834),
            http("getOrgDetailsBatch")
			.post("/dhs/ol/data/services/xsodata/ol.xsodata/$batch")
			.headers(headers_835)
			.body(RawFileBody("locator/locatornearby1/GETOrgDetails.dat"))))
		.pause(4)
		.exec(http("GET200km")
			.post("/dhs/ol/data/services/xsodata/ol.xsodata/$batch")
			.headers(headers_837)
			.body(RawFileBody("locator/locatornearby1/GET200km.dat")))
	}
	if (injectionProfile.equals("constantUser")) {
		setUp(scn.inject(constantUsersPerSec(users) during (duration seconds))).protocols(httpProtocol)
	} else if (injectionProfile.equals("heavyUser")) {
		setUp(scn.inject(heavisideUsers(users) during(duration minutes))).protocols(httpProtocol)
	} else if (injectionProfile.equals("rampUser")) {
		setUp(scn.inject(rampConcurrentUsers(users) to(userRamp) during(duration minutes))).protocols(httpProtocol)
	}
}
