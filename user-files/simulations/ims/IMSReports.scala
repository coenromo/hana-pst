package ims

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class IMSReports extends Simulation {

    //environment variables
	var injectionProfile = System.getenv("sInjectionProfile")

	var userString = System.getenv("gUsers")
	var durationString = System.getenv("gDuration")
	var userRampString = System.getenv("gRamp")

	var users = Integer.parseInt(userString)
	var duration = Integer.parseInt(durationString)
	var userRamp = Integer.parseInt(userRampString)

	// hide password
	var password = System.getenv("gPass")

	val httpProtocol = http
		.baseUrl("url-link")
		.inferHtmlResources(BlackList(""".*.css""", """.*.ico"""), WhiteList())
		.acceptHeader("text/plain, */*; q=0.01")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("en-US,en;q=0.5")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0")

	val headers_0 = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_3 = Map("Accept" -> "*/*")

	val headers_4 = Map(
		"Accept" -> "application/json",
		"X-CSRF-Token" -> "fetch")

	val headers_9 = Map("Accept" -> "text/css,*/*;q=0.1")

	val headers_10 = Map("X-Requested-With" -> "XMLHttpRequest")

	val headers_11 = Map(
		"Accept" -> "application/json, text/javascript, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_16 = Map(
		"Accept" -> "application/atomsvc+xml;q=0.8, application/json;odata=fullmetadata;q=0.7, application/json;q=0.5, */*;q=0.1",
		"MaxDataServiceVersion" -> "3.0")

	val headers_17 = Map("Accept" -> "image/webp,*/*")

	val headers_18 = Map(
		"Accept" -> "application/font-woff2;q=1.0,application/font-woff;q=0.9,*/*;q=0.8",
		"Accept-Encoding" -> "identity")

	val headers_33 = Map(
		"Accept" -> "application/xml, text/xml, */*; q=0.01",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_35 = Map(
		"Accept" -> "application/json, text/javascript, */*; q=0.01",
		"X-CSRF-Token" -> "Fetch",
		"X-Requested-With" -> "XMLHttpRequest")

	val headers_38 = Map(
		"Accept" -> "application/xml",
		"Accept-Language" -> "en",
		"MaxDataServiceVersion" -> "3.0",
		"sap-contextid-accept" -> "header")

	val headers_66 = Map(
		"Accept" -> "application/json",
		"Accept-Language" -> "en",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header",
		"x-csrf-token" -> "Fetch")

	val headers_67 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_aee8-4bc3-8fe1",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_68 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_bad0-1fde-51f1",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_86 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_5e56-0417-696b",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_92 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_adb9-e701-817d",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_93 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_4d9c-eb6d-7d30",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_94 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_afea-3329-d862",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_150 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_722c-1a00-a1fb",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_155 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_e8da-1426-fc23",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_158 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_4a55-2873-b9c5",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_161 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_7338-e93b-e9fc",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_164 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_8f67-9464-4c66",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_167 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_3327-0f09-9bc9",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_170 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_348c-8c24-c10a",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_175 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_e988-eb08-e995",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_176 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_b7da-8902-c3cb",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_179 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_51eb-c0d1-2bed",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")

	val headers_183 = Map(
		"Accept" -> "multipart/mixed",
		"Accept-Language" -> "en",
		"Content-Type" -> "multipart/mixed;boundary=batch_e9cf-5ab1-b29f",
		"DataServiceVersion" -> "2.0",
		"MaxDataServiceVersion" -> "2.0",
		"Origin" -> "url-link",
		"sap-cancel-on-close" -> "true",
		"sap-contextid-accept" -> "header")



	val scn = scenario("IMSReports")
		// Logging In And Loading
		.group("Loading IMS"){
			exec(http("Launchpad")
				.get("/sap/hana/uis/clients/ushell-app/shells/fiori/NewFioriLaunchpad.html?siteId=dhs|foh|ui|uis|appsites|foh")
				.headers(headers_0)
				.basicAuth("user-name",password)
				.resources(http("Loading IMS")
				.get("/sap/hana/uis/clients/flp/bootstrap/uis_fiori.js?ts=201607271029")
				.headers(headers_3),
				http("uis_nav_data.xsodata")
				.get("/sap/hana/uis/odata/uis_nav_data.xsodata/UIS_US_NAVIGATIONParameters(IN_TAG='ims-ims')/Results?$format=json&$top=1")
				.headers(headers_16),
				http("component-preload.js")
				.get("/dhs/ims/web/resources/Component-preload.js")
				.headers(headers_3),
				http("Component.js")
				.get("/dhs/ims/web/resources/Component.js?ts=201607271029")
				.headers(headers_10),
				http("manifest.json")
				.get("/dhs/ims/web/resources/manifest.json?ts=201607271029")
				.headers(headers_11),
				http("messageBundle_en.properties")
				.get("/dhs/ims/web/resources/i18n/messageBundle_en.properties")
				.headers(headers_10),
				http("app.view")
				.get("/dhs/ims/web/resources/view/App.view.xml?ts=201607271029")
				.headers(headers_33),
				http("config.xsodata")
				.get("/dhs/ims/js/lib/services/xsjs/config.xsjs?ts=201607271029")
				.headers(headers_11),
				http("wizard.xsodata")
				.get("/dhs/ims/js/lib/services/odata/wizard.xsodata/$metadata?sap-language=EN")
				.headers(headers_38),
				http("config.xsodata")
				.get("/dhs/ims/js/lib/services/odata/config.xsodata/$metadata?sap-language=EN")
				.headers(headers_38),
				http("main.view")
				.get("/dhs/ims/web/resources/view/Main.view.xml?ts=201607271029")
				.headers(headers_33),
				http("main.controller")
				.get("/dhs/ims/web/resources/controller/Main.controller.js?ts=201607271029")
				.headers(headers_10),
				http("base.controller")
				.get("/dhs/ims/web/resources/controller/Base.controller.js?ts=201607271029")
				.headers(headers_10),
				http("formatter.js")
				.get("/dhs/ims/web/resources/utility/formatter.js?ts=201607271029")
				.headers(headers_10),
				http("report.xsodata")
				.get("/dhs/ims/js/lib/services/odata/report.xsodata/$metadata?sap-language=EN")
				.headers(headers_38),
				http("site.xsodata")
				.get("/dhs/ims/js/lib/services/odata/site.xsodata/$metadata?sap-language=EN")
				.headers(headers_38),
				http("translation.xsodata")
				.get("/dhs/ims/js/lib/services/odata/translation.xsodata/$metadata?sap-language=EN")
				.headers(headers_38),
				http("Search.view")
				.get("/dhs/ims/web/resources/components/job/view/Search.view.xml?ts=201607271029")
				.headers(headers_33),
				http("Search.controller")
				.get("/dhs/ims/web/resources/components/job/controller/Search.controller.js?ts=201607271029")
				.headers(headers_10),
				http("vendor.xsodata")
				.get("/dhs/ims/js/lib/services/odata/vendor.xsodata/$metadata?sap-language=EN")
				.headers(headers_38),
				http("vcm.xsodata")
				.get("/dhs/ims/js/lib/services/odata/vcm.xsodata/$metadata?sap-language=EN")
				.headers(headers_38),
				http("TablePersoService.js")
				.get("/dhs/ims/web/resources/utility/TablePersoService.js?ts=201607271029")
				.headers(headers_10),
				http("Search.fragment")
				.get("/dhs/ims/web/resources/components/job/fragment/search/Search.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("SearchItem.fragment")
				.get("/dhs/ims/web/resources/components/job/fragment/search/SearchItem.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("job.xsodata")
				.get("/dhs/ims/js/lib/services/odata/job.xsodata/$metadata?sap-language=EN")
				.headers(headers_38),
				http("ResultTable.fragment")
				.get("/dhs/ims/web/resources/components/job/fragment/search/ResultTable.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("SearchConfig.js")
				.get("/dhs/ims/web/resources/components/job/data/SearchConfig.json?ts=201607271029")
				.headers(headers_11),
				http("config.xsodata")
				.get("/dhs/ims/js/lib/services/odata/config.xsodata/")
				.headers(headers_66),
				http("config.xsodata/ActiveCodes")
				.post("/dhs/ims/js/lib/services/odata/config.xsodata/$batch")
				.headers(headers_67)
				.body(RawFileBody("ims/imsreports/0067_request.dat")),
				http("config.xsodata/RecentChanges")
				.post("/dhs/ims/js/lib/services/odata/config.xsodata/$batch")
				.headers(headers_68)
				.body(RawFileBody("ims/imsreports/0068_request.dat"))))
		}
		
		.pause(3)
		// Additional Reports
		.group("Additional Reports"){
			exec(http("reports.view")
				.get("/dhs/ims/web/resources/components/reports/view/Reports.view.xml?ts=201607271029")
				.headers(headers_33)
				.resources(http("reports.controller")
				.get("/dhs/ims/web/resources/components/reports/controller/Reports.controller.js?ts=201607271029")
				.headers(headers_10),
				http("formatter.js")
				.get("/dhs/ims/web/resources/components/reports/util/formatter.js?ts=201607271029")
				.headers(headers_10),
				http("reportConfig.json")
				.get("/dhs/ims/web/resources/components/reports/model/reportConfig.json?ts=201607271029")
				.headers(headers_11),
				http("AdditionalReports.controller")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/AdditionalReports.controller.js?ts=201607271029")
				.headers(headers_10),
				http("AdditionalReports.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/AdditionalReports/AdditionalReports.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("VendorStats.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/AdditionalReports/VendorStats.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("VendorTotals.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/AdditionalReports/VendorTotals.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("InterpreterTotals.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/AdditionalReports/InterpreterTotals.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("ActiveInterpreterTotals.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/AdditionalReports/ActiveInterpreterTotals.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("TranslatorTotals.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/AdditionalReports/TranslatorTotals.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("ActiveTranslatorTotals.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/AdditionalReports/ActiveTranslatorTotals.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("JobReports.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/AdditionalReports/JobReports.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("JobTypeSummary.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/AdditionalReports/JobTypeSummary.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("RecentJobs.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/AdditionalReports/RecentJobs.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("report.xsodata")
				.get("/dhs/ims/js/lib/services/odata/report.xsodata/")
				.headers(headers_66),
				http("report.xsodata/ vendorSummary, interpreterSummary, activeInterpreters, translatorSummary, activeTranslators, recentJobsParameters, jobTypeSummaryParameters")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_86)
				.body(RawFileBody("ims/imsreports/0086_request.dat"))))
		}
		
		.pause(3)
		
		.group("Interpreter Coverage Report"){
			// Interpreter Coverage Report
			exec(http("InterpreterCoverage.controller")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/InterpreterCoverage.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("reports util.js")
				.get("/dhs/ims/web/resources/components/reports/util/util.js?ts=201607271029")
				.headers(headers_10),
				http("InterpreterCoverage.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/InterpreterCoverage/InterpreterCoverage.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("SearchItem.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/InterpreterCoverage/SearchItem.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("ResultTable.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/InterpreterCoverage/ResultTable.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("report.xsodata/states")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_92)
				.body(RawFileBody("ims/imsreports/0092_request.dat"))))
			.pause(1)

			.exec(http("report.xsodata/interpreterCoverage")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_93)
				.body(RawFileBody("ims/imsreports/0093_request.dat"))
				.resources(http("report.xsodata/interpreterCoverage")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_94)
				.body(RawFileBody("ims/imsreports/0094_request.dat"))))
		}
		
		.pause(3)

		.group("Job Allocation NAATI Report") {
			// Job Allocation NAATI Report
			exec(http("obNAATI.controller")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/JobNAATI.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("JobNAATI.fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/JobNAATI.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("report.xsodata/states, jobtypes, jobNAATIparameters")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_150)
				.body(RawFileBody("ims/imsreports/0150_request.dat"))))
		}
		
		.pause(3)

		.group("Job Count Summary"){
			// Job Count Summary 
			exec(http("JobCount.controller")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/JobCount.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("JobCount fragment")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/JobCount.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("report.xsodata/languageGroups, jobCountParameters")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_155)
				.body(RawFileBody("ims/imsreports/0155_request.dat"))))
		}
		
		.pause(3)

		.group("Job Statuses Report"){
			// Job Statuses Report
			exec(http("JobStatuses.controller.js")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/JobStatuses.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("JobStatuses.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/JobStatuses.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("report.xsodata/jobStatusesParameters")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_158)
				.body(RawFileBody("ims/imsreports/0158_request.dat"))))
		}
		
		.pause(3)

		.group("Jobs Created Report"){
			// Jobs Created Report
			exec(http("JobsCreated.controller.js")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/JobsCreated.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("JobsCreated.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/JobsCreated.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("report.xsodata/jobsCreatedParameters")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_161)
				.body(RawFileBody("ims/imsreports/0161_request.dat"))))

		}
		
		.pause(3)

		.group("Jobs Due Summary"){
			// Jobs Due Summary
			exec(http("JobsDue.controller.js")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/JobsDue.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("JobsDue.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/JobsDue.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("report.xsodata/jobsDueParameters")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_164)
				.body(RawFileBody("ims/imsreports/0164_request.dat"))))
		}

		.pause(3)
		
		.group("Org Job Distribution Report"){
			// Org Job Distribution Report
			exec(http("JobDistribution.controller.js")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/JobDistribution.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("JobDistribution.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/JobDistribution.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("report.xsodata/jobDistributionParameters")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_167)
				.body(RawFileBody("ims/imsreports/0167_request.dat"))))
		}
		
		.pause(3)

		.group("Management Item Summary"){
			// Management Item Summary
			exec(http("VCMItemSummary.controller.js")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/VCMItemSummary.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("VCMItemSummary.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/VCMItemSummary.fragment.xml?ts=201607271029")
				.headers(headers_33),
        	    http("report.xsodata/itemsummaryParameters")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_170)
				.body(RawFileBody("ims/imsreports/0170_request.dat"))))
		}

		.pause(3)

		.group("Rescheduled Jobs Report"){
			// Rescheduled Jobs Report
			exec(http("rescheduledjobs.controller.js")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/RescheduledJobs.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("RescheduledJobs.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/RescheduledJobs/RescheduledJobs.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("SearchItem.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/RescheduledJobs/SearchItem.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("ResultTable.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/RescheduledJobs/ResultTable.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("report.xsodata/jobSources, jobTypes, confirmations")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_175)
				.body(RawFileBody("ims/imsreports/0175_request.dat"))))
				.pause(1)
				// Rescheduled jobs Report Search
				exec(http("report.xsodata/rescheduledJobSearch")
					.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
					.headers(headers_176)
					.body(RawFileBody("ims/imsreports/0176_request.dat")))
		}
		
		.pause(3)

		.group("Unallocated Summary"){
			// Unallocated Summary
			exec(http("UnallocatedSummary.controller.js")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/UnallocatedSummary.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("UnallocatedSummary.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/UnallocatedSummary.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("report.xsodata/unallocatedSummary")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_179)
				.body(RawFileBody("ims/imsreports/0179_request.dat"))))
		}

		.pause(3)
		
		.group("User Allocations Report"){
			// User Allocations Report
			exec(http("UserAllocations.controller.js")
				.get("/dhs/ims/web/resources/components/reports/controller/fragments/UserAllocations.controller.js?ts=201607271029")
				.headers(headers_10)
				.resources(http("UserAllocations.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/UserAllocations/UserAllocations.fragment.xml?ts=201607271029")
				.headers(headers_33),
				http("ResultTable.fragment.xml")
				.get("/dhs/ims/web/resources/components/reports/view/fragments/UserAllocations/ResultTable.fragment.xml?ts=201607271029")
				.headers(headers_33)))
			.pause(1)
			// User Allocations Report Search
			.exec(http("report.xsodata/userAllocationsParameters")
				.post("/dhs/ims/js/lib/services/odata/report.xsodata/$batch")
				.headers(headers_183)
				.body(RawFileBody("ims/imsreports/0183_request.dat")))
		}

	if (injectionProfile.equals("constantUser")) {
		setUp(scn.inject(constantUsersPerSec(users) during (duration seconds))).protocols(httpProtocol)
	} else if (injectionProfile.equals("heavyUser")) {
		setUp(scn.inject(heavisideUsers(users) during(duration minutes))).protocols(httpProtocol)
	} else if (injectionProfile.equals("rampUser")) {
		setUp(scn.inject(rampConcurrentUsers(users) to(userRamp) during(duration minutes))).protocols(httpProtocol)
	}
}
