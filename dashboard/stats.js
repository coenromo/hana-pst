                                                                          
/*                                                                                
*              ___                   ___                                         
*              ,--.'|_               ,--.'|_                                       
*              |  | :,'              |  | :,'                     .--.             
*    .--.--.   :  : ' :              :  : ' :  .--.--.          .--,`|  .--.--.    
*   /  /    '.;__,'  /    ,--.--.  .;__,'  /  /  /    '         |  |.  /  /    '   
*  |  :  /`./|  |   |    /       \ |  |   |  |  :  /`./         '--`_ |  :  /`./   
*  |  :  ;_  :__,'| :   .--.  .-. |:__,'| :  |  :  ;_           ,--,'||  :  ;_     
*   \  \    `. '  : |__  \__\/: . .  '  : |__ \  \    `.        |  | ' \  \    `.  
*    `----.   \|  | '.'| ," .--.; |  |  | '.'| `----.   \       :  | |  `----.   \ 
*   /  /`--'  /;  :    ;/  /  ,.  |  ;  :    ;/  /`--'  /__   __|  : ' /  /`--'  / 
*  '--'.     / |  ,   /;  :   .'   \ |  ,   /'--'.     /  .\.'__/\_: |'--'.     /  
*    `--'---'   ---`-' |  ,     .-./  ---`-'   `--'---'\  ; |   :    :  `--'---'   
*                       `--`---'                        `--" \   \  /              
*                                                             `--`-'               
*
* JS Script to find stats and display them on the index.html
* very quick and dirty :(
*/

//Declare Variables OLD
var max1, max2, max3, max4, max5, max6, max7, max8, max9, max0, desc1, desc2, desc3, desc4, desc5, desc6, desc7, desc8, desc9, desc0;
var maxIMSReportsNew, maxIMSReportsOld;
var scenRe = new RegExp('InjectionProfile=(\\w+)')
var durRe = new RegExp('Duration=(\\d+)');
var usersRe = new RegExp('Users=(\\d+)');
var rampRe = new RegExp('Ramp=(\\d+)');
var dateCur = new RegExp(/Date\=.*/);

/**  --------foh-current---------
 * Get Environment Variables from run job
 * @param {JSON} data       JSON containing Env Var
 */
$.getJSON("./foh-pst/js/assertions.json", function (data) {
    desc6 = data.description;
    envArrCur = [];
    envArrCur.push(scenRe.exec(desc6)[1], durRe.exec(desc6)[1], usersRe.exec(desc6)[1], rampRe.exec(desc6)[1], dateCur.exec(desc6)[0]);


    /**
     * Get Statistics from run job and publish to page
     * @param {JSON} data       JSON containing Stats
     */
    $.getJSON("./foh-pst/js/stats.json", function (data) {
        max7 = data.stats.maxResponseTime.total;
        var av1 = data.stats.meanResponseTime.total;
        var failed2 = data.stats.group4.count;
        var average = data.stats.meanResponseTime.total;
        document.getElementById("foh-pst").innerHTML = '<i>' + 'Profile - ' + envArrCur[0] + ', Duration - ' + envArrCur[1] + ' min, Users - ' + envArrCur[2] + ', Ramp - ' + envArrCur[3] + '</i>' + '<hr>' + 'FOH - ' + max7 + ' ms max :: ' + av1 + 'ms average';
        // replace "Date=" in string with empty
        var dateRegex = envArrCur[4];
        var dateTrim = dateRegex.replace("Date=", ""); 
        document.getElementById("currentMain").innerHTML = 'Current run - ' + dateTrim;
        

        //check if responses failed
        if (failed2 > 0) {
            console.log("failed", failed2);
            document.getElementById("failPassFohCurrent").innerHTML += ' - ⚠️ Tests failed!';
        } else {
            console.log("passed", failed2);
            document.getElementById("failPassFohCurrent").innerHTML += ' - 👍 Tests Successful';
        }

        //std old
        $.getJSON("./foh-old/js/stats.json", function (data) {
            var averageOld = data.stats.meanResponseTime.total;
            var std = data.stats.standardDeviation.total;

            if (average > averageOld + std) {
                console.log("bad average against previous test results", average, averageOld);
                document.getElementById("fohstd").innerHTML = 'Results: <br> ❌';
                document.getElementById("fohstdtext").innerHTML = 'Response times have increased since last test run - please check results';
            } else {
                console.log("NOICE, you did not break anything", average, averageOld);
                document.getElementById("fohstd").innerHTML = 'Results: <br> ✔️';
                document.getElementById("fohstdtext").innerHTML = 'Response times within acceptable bounds.';
            }
        });
    });
});

/**   ----------foh----------
 * Get Environment Variables from run job
 * @param {JSON} data       JSON containing Env Var
 */
$.getJSON("./foh-old/js/assertions.json", function (data) {
    desc1 = data.description;
    envArrFohOld = [];
    envArrFohOld.push(scenRe.exec(desc1)[1], durRe.exec(desc1)[1], usersRe.exec(desc1)[1], rampRe.exec(desc1)[1], dateCur.exec(desc1)[0]);
    // replace "Date=" in string with empty
    var dateRegexOld = envArrFohOld[4];
    var dateTrimOld = dateRegexOld.replace("Date=", ""); 
    document.getElementById("previousMain").innerHTML = 'Previous run - ' + dateTrimOld;

    /**
     * Get Statistics from run job and publish to page
     * @param {JSON} data       JSON containing Stats
     */
    $.getJSON("./foh-old/js/stats.json", function (data) {
        max2 = data.stats.maxResponseTime.total;
        var av = data.stats.meanResponseTime.total;
        var failed = data.stats.group4.count;
        document.getElementById("foh-old").innerHTML = '<i>' + 'Profile - ' + envArrFohOld[0] + ', Duration - ' + envArrFohOld[1] + ' min, Users - ' + envArrFohOld[2] + ', Ramp - ' + envArrFohOld[3] + '</i>' + '<hr>' + 'FOH - ' + max2 + 'ms max :: ' + av + 'ms average';

        //check if responses failed
        if (failed > 0) {
            console.log("failed", failed);
            document.getElementById("failPassFohOld").innerHTML += ' - ⚠️ Tests failed!';
        } else {
            console.log("passed", failed);
            document.getElementById("failPassFohOld").innerHTML += ' - 👍 Tests Successful';
        }
    });
});

/**
 * Old Results - Locator
 * Get Environment Variables from run job
 * @param {JSON} data       JSON containing Env Var
 */
$.getJSON("./LocatorTest1-old/js/assertions.json", function (data) {
    desc3 = data.description;
    envArrOlOld = [];    
    envArrOlOld.push(scenRe.exec(desc3)[1], durRe.exec(desc3)[1], usersRe.exec(desc3)[1], rampRe.exec(desc3)[1]);

    /**
     * Get Statistics from run job and publish to page
     * @param {JSON} data       JSON containing Stats
     */
    $.getJSON("./LocatorTest1-old/js/stats.json", function (data) {
        max3 = data.stats.maxResponseTime.total;
        var avLocOld = data.stats.meanResponseTime.total;
        var failedLocOld = data.stats.group4.count;
        document.getElementById("LocatorTest1Old").innerHTML = '<i>' + 'Profile - ' + envArrOlOld[0] + ', Duration - ' + envArrOlOld[1] + ' min, Users - ' + envArrOlOld[2] + ', Ramp - ' + envArrOlOld[3] + '</i>' + '<hr>' + 'Search - ' + max3 + ' ms max :: ' + avLocOld + 'ms average';

        //check if responses failed
        if (failedLocOld > 0) {
            console.log("failed", failedLocOld);
            document.getElementById("failPassLocOld").innerHTML += ' - ⚠️ Tests failed!';
        } else {
            console.log("passed", failedLocOld);
            document.getElementById("failPassLocOld").innerHTML += ' - 👍 Tests Successful';
        }

    });
});
/**
 * New Results - Locator
 * Get Environment Variables from run job
 * @param {JSON} data       JSON containing Env Var
 */
$.getJSON("./LocatorTest1-pst/js/assertions.json", function (data) {
    desc3 = data.description;
    envArrOl = [];
    envArrOl.push(scenRe.exec(desc3)[1], durRe.exec(desc3)[1], usersRe.exec(desc3)[1], rampRe.exec(desc3)[1]);

    /**
     * Get Statistics from run job and publish to page
     * @param {JSON} data       JSON containing Stats
     */
    $.getJSON("./LocatorTest1-pst/js/stats.json", function (data) {
        max8 = data.stats.maxResponseTime.total;
        var avLoc = data.stats.meanResponseTime.total;
        var failedLoc = data.stats.group4.count;
        document.getElementById("LocatorTest1").innerHTML = '<i>' + 'Profile - ' + envArrOl[0] + ', Duration - ' + envArrOl[1] + ' min, Users - ' + envArrOl[2] + ', Ramp - ' + envArrOl[3] + '</i>' + '<hr>' + 'Search - ' + max8 + ' ms max :: ' + avLoc + 'ms average';

        //check if responses failed
        if (failedLoc > 0) {
            console.log("failed", failedLoc);
            document.getElementById("failPassLoc").innerHTML += ' - ⚠️ Tests failed!';
        } else {
            console.log("passed", failedLoc);
            document.getElementById("failPassLoc").innerHTML += ' - 👍 Tests Successful';
        }

        //std old locator
        $.getJSON("./LocatorTest1-old/js/stats.json", function (data) {
            var averageOldLoc = data.stats.meanResponseTime.total;
            var stdLoc = data.stats.standardDeviation.total;

            if (avLoc > averageOldLoc + stdLoc) {
                console.log("bad average against previous test results", avLoc, averageOldLoc);
                document.getElementById("locstd").innerHTML += 'Results <br> ❌';
                document.getElementById("locstdtext").innerHTML = 'Response times have increased since last test run - check results';
            } else {
                console.log("NOICE, you did not break anything", avLoc, averageOldLoc);
                document.getElementById("locstd").innerHTML = 'Results: <br> ✔️';
                document.getElementById("locstdtext").innerHTML = 'Response times within acceptable bounds.';
            }
        });
    });
});

/**
 * Setting environment variables and description for IMS reports
 * @param {JSON} data       JSON containing Env Var
 */
//extracting simulation parameters
$.getJSON("./ims-reports-pst/js/assertions.json", function (data) {
    var descIMSReportsNew = data.description;
    envArrIms = [];
    envArrIms.push(scenRe.exec(descIMSReportsNew)[1], durRe.exec(descIMSReportsNew)[1], usersRe.exec(descIMSReportsNew)[1], rampRe.exec(descIMSReportsNew)[1]);
    $.getJSON("./ims-reports-pst/js/stats.json", function (data) {
        maxIMSReportsNew = data.stats.maxResponseTime.total;
        document.getElementById("ims-reports").innerHTML = '<i>' + 'Profile - ' + envArrIms[0] + ', Duration - ' + envArrIms[1] + ' min, Users - ' + envArrIms[2] + ', Ramp - ' + envArrIms[3] + '</i>' + '<hr>' + 'Batch - ' + maxIMSReportsNew + ' ms max';
    });
});

$.getJSON("./ims-reports-old/js/assertions.json", function (data) {
    var descIMSReportsOld = data.description;
    envArrImsOld = [];
    envArrImsOld.push(scenRe.exec(descIMSReportsOld)[1], durRe.exec(descIMSReportsOld)[1], usersRe.exec(descIMSReportsOld)[1], rampRe.exec(descIMSReportsOld)[1]);
    $.getJSON("./ims-reports-old/js/stats.json", function (data) {
        maxIMSReportsOld = data.stats.maxResponseTime.total;
        document.getElementById("ims-reportsOld").innerHTML = '<i>' + 'Profile - ' + envArrImsOld[0] + ', Duration - ' + envArrImsOld[1] + ' min, Users - ' + envArrImsOld[2] + ', Ramp - ' + envArrImsOld[3] + '</i>' + '<hr>' + 'Batch - ' + maxIMSReportsOld + ' ms max';
    });
});
