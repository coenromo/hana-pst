        ______    ______   ________  __        ______  __    __   ______         _______    ______   ________ 
    /      \  /      \ /        |/  |      /      |/  \  /  | /      \       /       \  /      \ /        |
    /$$$$$$  |/$$$$$$  |$$$$$$$$/ $$ |      $$$$$$/ $$  \ $$ |/$$$$$$  |      $$$$$$$  |/$$$$$$  |$$$$$$$$/ 
    $$ | _$$/ $$ |__$$ |   $$ |   $$ |        $$ |  $$$  \$$ |$$ | _$$/       $$ |__$$ |$$ \__$$/    $$ |   
    $$ |/    |$$    $$ |   $$ |   $$ |        $$ |  $$$$  $$ |$$ |/    |      $$    $$/ $$      \    $$ |   
    $$ |$$$$ |$$$$$$$$ |   $$ |   $$ |        $$ |  $$ $$ $$ |$$ |$$$$ |      $$$$$$$/   $$$$$$  |   $$ |   
    $$ \__$$ |$$ |  $$ |   $$ |   $$ |_____  _$$ |_ $$ |$$$$ |$$ \__$$ |      $$ |      /  \__$$ |   $$ |   
    $$    $$/ $$ |  $$ |   $$ |   $$       |/ $$   |$$ | $$$ |$$    $$/       $$ |      $$    $$/    $$ |   
    $$$$$$/  $$/   $$/    $$/    $$$$$$$$/ $$$$$$/ $$/   $$/  $$$$$$/        $$/        $$$$$$/     $$/  

_Under Construction_

# Gatling Stress Test Tool for T3D Applications

Example home page hosted on Gitlab Pages because all of the data routes have been removed for security. 

 :computer: :computer: :computer: ![alt text](img/HANA-PST-Home-Page.png) :computer: :computer: :computer:

## Tools currently being tested

* Front of House
* Interpreter Management System
* Locator
* Customer Incident Management System

## Gatling Setup

* Install Java SDK from [Java SE Development Kit 13](https://www.oracle.com/java/technologies/javase-jdk13-downloads.html)
* Add your install path (C:\Program Files\Java\java-sdk-13.0.2\bin) to the %PATH% environment variable (If java installer doesn't do it for you)
* In CMD, check Java installed properly by typing `java - version`
* Add a new environment variable %JAVA_HOME% and set that to the jave path (C:\Program Files\Java\java-sdk-13.0.2)
* Download Gatling from [Gatling Download](https://gatling.io/open-source) __or__ clone this GitLab repository
* Download Firefox
* Set Gatling settings to ![alt text](img/gatling.png)
* Set Firefox Network Settings to ![alt text](img/firefox.png)
* Start Gatling
* In Firefox, browse to the app url and start clicking things
* Once complete, stop the Gatling UI
* Run `Gatling > bin > gatling.bat` and select your scala test
